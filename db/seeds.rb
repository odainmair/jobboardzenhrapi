# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'database_cleaner'

DatabaseCleaner.clean_with(:truncation)

1.times do
    user = User.create! email: Faker::Internet.email, password: 'password',role:'admin'
    puts "Created a new user: #{user.email}"
  
    3.times do
      job = Job.create!(
        title: Faker::Commerce.product_name,
        description: Faker::Commerce.product_name,
        user_id: user.id
      )
      puts "Created a  new job: #{job.title}"
    end
  end

  1.times do
    user = User.create! email: Faker::Internet.email, password: 'password',role:'member'
    puts "Created a new user: #{user.email}"
  
    3.times do
      job = Job.create!(
        title: Faker::Commerce.product_name,
        description: Faker::Commerce.product_name,
        user_id: user.id
      )
      puts "Created a  new job: #{job.title}"
    end
  end 
