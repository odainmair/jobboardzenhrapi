require 'rails_helper'

# Test suite for the Job model
RSpec.describe Job, type: :model do
  # Association test

  # Validation tests
  # ensure columns title and created_by are present before saving
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:description) }
end