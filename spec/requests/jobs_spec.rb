require 'rails_helper'

RSpec.describe 'Jobs API', type: :request do
  # initialize test data
  let(:user) { create(:user) }
  let!(:jobs) { create_list(:job, 10, user_id: user.id) }
  let(:job_id) { jobs.first.id }
  let(:headers) { valid_headers }
  let!(:job) { create(:job,user_id: user.id) }
  let(:valid_request) do
    { title: job.title, description: job.description }.to_json
  end
  # Test suite for GET /jobs
  describe 'GET /jobs' do
    # make HTTP get request before each example
    before { get '/api/v1/jobs', params: {}, headers: headers  }

    it 'returns jobs' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /jobs/:id
  describe 'GET /jobs/:id' do
    before { get "/api/v1/jobs/#{job_id}" , params: {}, headers: headers  }

    context 'when the record exists' do
      it 'returns the job' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(job_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:job_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find job/)
      end
    end
  end

  # Test suite for POST /jobs
  describe 'POST /jobs' do
    # valid payload
    let(:valid_attributes) { {title: 'Learn Elm', description: 'FFF FFF' }}

    context 'when the request is valid' do
      before { post '/api/v1/jobs', params: valid_request, headers: headers }

      it 'creates a job' do
        expect(json['data']['attributes']['title']).to eq('Learn Elm')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/v1/jobs', params: { title: 'Foobar' }, headers: headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Created by can't be blank/)
      end
    end
  end

  # Test suite for PUT /jobs/:id
  describe 'PUT /jobs/:id' do
    let(:valid_attributes) { { title: 'Shopping' } }

    context 'when the record exists' do
      before { put "/api/v1/jobs/#{job_id}", params: valid_attributes, headers: headers }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  # Test suite for DELETE /jobs/:id
  describe 'DELETE /jobs/:id' do
    before { delete "/api/v1/jobs/#{job_id}", headers: headers }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end