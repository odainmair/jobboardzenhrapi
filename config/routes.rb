Rails.application.routes.draw do
  
  namespace :api, defaults: { format: :json } do
    namespace :v1 do

      resources :tokens, only: [:create]
      resources :jobs
      resources :applications
    end
  end
  post 'auth/signup', to: 'users#create'
  post 'auth/login', to: 'authentication#authenticate'
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  
end
