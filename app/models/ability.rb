require 'cancancan'

class Ability
  include CanCan::Ability

  def initialize(user)
    send("#{user.role}_abilities", user)
  end

  def admin_abilities(user)
    can [:read, :update, :create], User
    can [:read, :update, :create], Job
    can [:read], Application
  end

  def member_abilities(user)
  
    can [:read, :update], User, { id: user.id }
    can [:read, :create], Application, { id: user.id }
    can  :read, Job
  end

  def visitor_abilities(user)
    can :read, Job
    can :create, User
  end

  def to_list
    rules.map do |rule|
      object = { action: rule.actions, subject: rule.subjects.map{ |s| s.is_a?(Symbol) ? s : s.name } }
      object[:conditions] = rule.conditions unless rule.conditions.blank?
      object[:inverted] = true unless rule.base_behavior
      object
    end
  end
end 
