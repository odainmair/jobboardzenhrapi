class User < ApplicationRecord
    validates :role, presence: true, inclusion: { in: %w{admin member visitor} }
    validates :email, uniqueness: true
    validates_format_of :email, with: /@/
    validates :password, presence: true
    has_many :jobs, dependent: :destroy
    has_many :applications, dependent: :destroy
    has_secure_password
    def role?(name)
        role == name.to_s
    end
    after_initialize :set_defaults, unless: :persisted?
    def set_defaults
      self.role  ||= 'member'
    end
    
    
end
