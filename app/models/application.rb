class Application < ApplicationRecord
  belongs_to :job
  belongs_to :user
  after_initialize :set_defaults, unless: :persisted?
  def set_defaults
    self.status  ||= 'Not Seen'
  end

  scope :recent, lambda {
    order(:updated_at)
  }


  def self.search(params = {})
    applications = params[:application_ids].present? ? Application.find(params[:application_ids]) : Application.all


    applications = applications.recent(params[:recent]) if params[:recent].present?

  end
end
