class Job < ApplicationRecord
  belongs_to :user
  validates :title, :user_id,:description, presence: true

  scope :filter_by_title, lambda { |keyword|
    where('lower(title) LIKE ?', "%#{keyword.downcase}%")
  }
  scope :recent, lambda {
    order(:updated_at)
  }


  def self.search(params = {})
    jobs = params[:job_ids].present? ? Job.find(params[:job_ids]) : Job.all

    jobs = jobs.filter_by_title(params[:keyword]) if params[:keyword]
    jobs = jobs.recent(params[:recent]) if params[:recent].present?

    jobs
  end
end
