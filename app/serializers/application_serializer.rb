class ApplicationSerializer
  include FastJsonapi::ObjectSerializer
  attributes :status
  belongs_to :user
  belongs_to :job
  cache_options enabled: true, cache_length: 12.hours
end
