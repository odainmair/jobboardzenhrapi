class JobSerializer
  include FastJsonapi::ObjectSerializer
  attributes :title, :description
  belongs_to :user
  cache_options enabled: true, cache_length: 12.hours
end
