class Api::V1::JobsController < ApplicationController
    include Paginable
    #skip_before_action :authorize_request, only: :show
    #skip_before_action :authorize_request, only: :index

    before_action :set_job, only: %i[show update destroy]
    before_action :check_login, only: %i[create]
    load_and_authorize_resource class: "Job"
    before_action :check_owner, only: %i[update destroy]
  
    def index
      @jobs = Job.includes(:user)
                         .page(current_page)
                         .per(per_page)
                         .search(params)
  
      options = get_links_serializer_options('api_v1_jobs_path', @jobs)
      options[:include] = [:user]
  
      render json: JobSerializer.new(@jobs, options).serializable_hash
    end
  
    def show
      options = { include: [:user] }
      render json: JobSerializer.new(@job, options).serializable_hash
    end
  
    def create
      job = current_user.jobs.build(job_params)
      if job.save
        render json: JobSerializer.new(job).serializable_hash, status: :created
      else
        render json: { errors: job.errors }, status: :unprocessable_entity
      end
    end
  
    def update
      if @job.update(job_params)
        render json: JobSerializer.new(@job).serializable_hash
      else
        render json: @job.errors, status: :unprocessable_entity
      end
    end
  
    def destroy
      @job.destroy
      head 204
    end
  
    private
  
    def check_owner
      head :forbidden unless @job.user_id == current_user&.id
    end
  
    def job_params
      params.permit(:title, :description)
    end
  
    def set_job
      @job = Job.find(params[:id])
    end
end
