class Api::V1::ApplicationsController < ApplicationController
    before_action :set_application, only: %i[show update destroy]
    before_action :check_login, only: %i[create index show]
   # load_and_authorize_resource class: "Application"

    include Paginable
    def index
        if current_user.role.eql? 'admin'
            @applications = Application.all.order("created_at desc")
            
        else
            @applications = current_user.applications.order("created_at desc")

        end    
      
       
        options = { include: [:user,:job] }
        render json: ApplicationSerializer.new(@applications, options).serializable_hash
    end
    def show
        options = { include: [:user,:job] }
        puts(current_user.role)
        if current_user.role.eql? 'admin'
            @application.update(status:'Seen')
        end
        render json: ApplicationSerializer.new(@application, options).serializable_hash
    end
    def create
        application = current_user.applications.build(application_params  )
        if application.save
            render json: ApplicationSerializer.new(application).serializable_hash, status: :created
        else
            render json: { errors: job.errors }, status: :unprocessable_entity
        end
    end
    def application_params
        params.require(:application).permit(:job_id) 
    end
    def set_application
        @application = Application.find(params[:id])
      end
end
