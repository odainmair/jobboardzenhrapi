class UsersController < ApplicationController
  before_action :set_user, only: %i[update]
  load_and_authorize_resource class: "User"


  def update
    if @user.update(user_params)
      render json: UserSerializer.new(@user).serializable_hash
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def create
    user = User.create!(user_params)
    auth_token = AuthenticateUser.new(user.email, user.password).call
    response = { message: Message.account_created, auth_token: auth_token }
    json_response(response, :created)
  end

 

  private


  # Only allow a trusted parameter "white list" through.
  def user_params
    params.permit(:email, :password)
  end

  def set_user
    @user = User.find(params[:id])
  end
end
