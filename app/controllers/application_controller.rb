class ApplicationController < ActionController::API
    include Response
    include Authenticable
    include ExceptionHandler

    include CanCan::ControllerAdditions
    rescue_from CanCan::AccessDenied do |exception|
        render json: { message: exception.message }, status: 403
    end

    #before_action :authorize_request
    #attr_reader :current_user

    #
    # PRIVATE
    #

    #private

    # Check for valid request token and return user
    #def authorize_request
      #@current_user = AuthorizeApiRequest.new(request.headers).call[:user]
    #end

end
